#!/bin/bash

today=$(date +%Y%m%d)
directory=$(pwd)/$today
log_file=$(pwd)/create_daily_folder.log

if [ $# == 0 ] # create directory in current directory
then 
	if [ -d $directory  ] # check if directory already exists
		then 
			echo "directory $directory already exists" >> $log_file
			exit 2

	else
		mkdir $directory
		echo "creating directory $directory" >> $log_file
	fi

elif [ $# == 1 ] # create directory in passed directory of arguments
then 
	directory=$1
       	directory="${directory%/}" # remove trailing / from passed directory, if present
	directory_today=$directory/$today
	log_file=$directory/create_daily_folder.log

	if [ ! -d $directory ] 
	      	then	echo "cannot create directory $directory_today because directory $directory does not exist" >> $log_file.
			exit 2
	elif [ -d $directory_today ]
		then 	echo "directory $directory_today already exists" >> $log_file
			exit 2
	else	
		mkdir $directory_today
		echo "creating directory $directory_today" >> $log_file
	fi

else 
	echo "too many arguments passed" >> $log_file
	exit 2
fi
